import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetBottomMinutesTest {

    GetBottomMinutes getBottomMinutes;

    @Before
    public void setup() {
        getBottomMinutes = new GetBottomMinutes();
    }

    @Test
    public void test16Minutes() {
        Assert.assertEquals("YOOO", getBottomMinutes.getBerlinDisplay(16));
    }

    @Test
    public void test20Minutes() {
        Assert.assertEquals("OOOO", getBottomMinutes.getBerlinDisplay(20));
    }

    @Test
    public void test59Minutes() {
        Assert.assertEquals("YYYY", getBottomMinutes.getBerlinDisplay(29));
    }

    @Test
    public void test3Minutes() {
        Assert.assertEquals("YYYO", getBottomMinutes.getBerlinDisplay(3));
    }
}
