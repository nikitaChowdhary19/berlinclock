import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetBottomHoursTest {

    GetBottomHours getBottomHours;

    @Before
    public void setup() {
        getBottomHours = new GetBottomHours();
    }

    @Test
    public void test20Hour() {
        Assert.assertEquals("OOOO", getBottomHours.getBerlinDisplay(20));
    }

    @Test
    public void test21Hour() {
        Assert.assertEquals("ROOO", getBottomHours.getBerlinDisplay(21));
    }

    @Test
    public void test22Hour() {
        Assert.assertEquals("RROO", getBottomHours.getBerlinDisplay(22));
    }

    @Test
    public void test23Hour() {
        Assert.assertEquals("RRRO", getBottomHours.getBerlinDisplay(23));
    }

    @Test
    public void test24Hour() {
        Assert.assertEquals("RRRR", getBottomHours.getBerlinDisplay(24));
    }
}
