import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GetTopMinutesTest {
    GetTopMinutes getTopMinutes;

    @Before
    public void setup() {
        getTopMinutes = new GetTopMinutes();
    }

    @Test
    public void test00Minutes() {
        Assert.assertEquals("OOOOOOOOOOO", getTopMinutes.getBerlinDisplay(0));
    }

    @Test
    public void test6Minutes() {
        Assert.assertEquals("YOOOOOOOOOO", getTopMinutes.getBerlinDisplay(6));
    }

    @Test
    public void test15Minutes() {
        Assert.assertEquals("YYROOOOOOOO", getTopMinutes.getBerlinDisplay(15));
    }

    @Test
    public void test20Minutes() {
        Assert.assertEquals("YYRYOOOOOOO", getTopMinutes.getBerlinDisplay(20));
    }

    @Test
    public void test59Hour() {
        Assert.assertEquals("YYRYYRYYRYY", getTopMinutes.getBerlinDisplay(59));
    }
}
