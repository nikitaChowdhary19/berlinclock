import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TimeParserTest {

    private TimeParser timeParser;

    @Before
    public void setup() {
        timeParser = new TimeParser();
    }

    @Test
    public void testValidTime() {
        String time = "23:12:34";
        List<Integer> result = timeParser.parse(time);
        assertEquals(3, result.size());
        assertTrue(result.contains(23));
        assertTrue(result.contains(12));
        assertTrue(result.contains(34));
    }

    @Test
    public void invalidTime() {
        String time = "40:59:59";
        List<Integer> result = timeParser.parse(time);
        assertEquals(0, result.size());
    }

    @Test
    public void incompleteTime() {
        String time = "40:59";
        List<Integer> result = timeParser.parse(time);
        assertEquals(0, result.size());
    }

    @Test
    public void incorrectTime() {
        String time = "ab:vc:12";
        List<Integer> result = timeParser.parse(time);
        assertEquals(0, result.size());
    }
}
