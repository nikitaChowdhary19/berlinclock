import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetTopHoursTest {
    GetTopHours getTopHours;

    @Before
    public void setup() {
        getTopHours = new GetTopHours();
    }

    @Test
    public void test00Hour() {
        Assert.assertEquals("OOOO", getTopHours.getBerlinDisplay(0));
    }

    @Test
    public void test5Hour() {
        Assert.assertEquals("ROOO", getTopHours.getBerlinDisplay(6));
    }

    @Test
    public void test10Hour() {
        Assert.assertEquals("RROO", getTopHours.getBerlinDisplay(13));
    }

    @Test
    public void test15Hour() {
        Assert.assertEquals("RRRO", getTopHours.getBerlinDisplay(15));
    }

    @Test
    public void test20Hour() {
        Assert.assertEquals("RRRR", getTopHours.getBerlinDisplay(22));
    }
}
