
public class GetTopMinutes extends AbstractConvertTimeToString {

    public String getBerlinDisplay(int time) {
        return getSigns(11, getTopNumberOfOnSigns(time), "Y").replaceAll("YYY", "YYR");
    }
}
