import java.util.LinkedList;
import java.util.List;


class TimeParser {

    List<Integer> parse(String time) {
        String[] timeSplit = time.split(":");

        List<Integer> timeList = new LinkedList<Integer>();
        if(timeSplit.length != 3) {
            System.out.println(String.format("Time %s has to be entered in hh:mm:ss format, please retry",
                    time));
            return timeList;
        }

        try {
            int hour = Integer.parseInt(timeSplit[0]);
            int min = Integer.parseInt(timeSplit[1]);
            int sec = Integer.parseInt(timeSplit[2]);
            if(validateTime(hour, min, sec)){
                timeList.add(Integer.parseInt(timeSplit[0]));
                timeList.add(Integer.parseInt(timeSplit[1]));
                timeList.add(Integer.parseInt(timeSplit[2]));
            }

        } catch(NumberFormatException e) {
            System.out.println(String.format("Time %s should be integer, please retry",
                    time));
        }

        return timeList;
    }

    private static boolean validateTime(int hour, int min, int sec) {
        if(hour > 24 || min >= 60 || sec >= 60) {
            System.out.println("Time entered is out of range, please retry");
            return false;
        }
        return true;
    }
}
