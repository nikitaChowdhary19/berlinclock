
abstract class AbstractConvertTimeToString {

    public abstract String getBerlinDisplay(int time);

    static String getSigns(int lamps, int onSigns) {
        return getSigns(lamps, onSigns, "R");
    }

    static String getSigns(int lamps, int totalOn, String symbol) {
        String out = "";
        for (int i = 0; i < totalOn; i++) {
            out += symbol;
        }
        for (int i = 0; i < (lamps - totalOn); i++) {
            out += "O";
        }
        return out;
    }

    static int getTopNumberOfOnSigns(int number) {
        return (number - (number % 5)) / 5;
    }
}
