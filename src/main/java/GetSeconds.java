/**
 * If the seconds is even the light is on which is represented by "Y"
 * else the light is off which is represented by "O"
 */
public class GetSeconds extends AbstractConvertTimeToString {

    public String getBerlinDisplay(int time) {
        if (time % 2 == 0) return "Y";
        else return "O";
    }
}
