import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class BerlinClock {

    public static void main(String[] args) throws IOException {
        TimeParser timeParser = new TimeParser();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            System.out.println("Enter time in hh:mm:ss format, or :quit to quit");
            String time = reader.readLine();
            if(time.equals(":quit")) {
                break;
            }
            List<Integer> timeSplit = timeParser.parse(time);
            if(timeSplit.size() != 3) {
                continue;
            }

            int hour = timeSplit.get(0);
            int min = timeSplit.get(1);
            int sec = timeSplit.get(2);

            System.out.println(new GetSeconds().getBerlinDisplay(sec));
            System.out.println(new GetTopHours().getBerlinDisplay(hour));
            System.out.println(new GetBottomHours().getBerlinDisplay(hour));
            System.out.println(new GetTopMinutes().getBerlinDisplay(min));
            System.out.println(new GetBottomMinutes().getBerlinDisplay(min));



        }
    }

}
