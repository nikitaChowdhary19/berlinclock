BerlinClock
Version - 1.0

=======================================================================================================================
--How to execute the Berlin Clock Display?--

** Install maven, you can find the instruction to install maven here http://maven.apache.org/install.html. If you have
homebrew you can use "brew install maven"
** Crete a new directory and move to the directory from the terminal. (Let's call it PACKAGE_ROOT)
** Run "git init" to initialize a git repository
** "git clone https://bitbucket.org/nikitaChowdhary19/berlinclock.git" to pull the code
** Navigate to the directory in which the code was cloned, to execute the next steps.
** (Make sure the current directory contains pom.xml) Run "mvn clean compile" to build the package
** Then run "mvn package", this will run the tests and build jar for the package.
** The jar should be present in the target folder, cd target and verify that the jar
is present there("berlin-clock-1.0.jar")
** change directory to classes "cd classes"
** The current directory should be <PACKAGE_ROOT>/target/classes
** To get the berlin clock display:
    java -jar ../berlin-clock-1.0.jar
    "You will be prompted to enter the time in some format."
    <Try with some time>
    Examples:
    ####################################################################################################################
    Enter time in hh:mm:ss format, or :quit to quit
    13:43:56
    Y
    RROO
    RRRO
    YYRYYRYYOOO
    YYYO
    ####################################################################################################################
    Enter time in hh:mm:ss format, or :quit to quit
    aa:nn 66
    Time aa:nn 66 has to be entered in hh:mm:ss format, please retry
    ####################################################################################################################
    Enter time in hh:mm:ss format, or :quit to quit
    345:678:21
    Time entered is out of range, please retry

    To exit from the console type ":quit"

--Understanding the result:--

The first row denotes seconds, Y => even seconds, O => odd seconds

The second row is the top hour which contains 4 block where each block represents 5 hours. R denotes the light is on,
and O denotes off. "RROO" in the first row reads 10 hours.

The third row is the bottom hour which has four blocks and each block denotes 1 hour. The light indication are same as
top hour row. "RRRO" in bottom hour row denotes 3 hours.
So in total it is 13 hours of time.

The fourth and fifth row for displaying minutes. The fourth row has 11 blocks, each block represents 5 min of time and
fifth row contains 5 block, each representing 1 min of time. On is yellow and off is O. R in fourth row denoting 15,
30 and 45 minutes past.
